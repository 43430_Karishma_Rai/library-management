#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"
void member_area(user_t *u){
    int choice;
    char name[80];
    do{
        printf("\n\n1. Find Book\n2. Edit profile\n3. Change Password\n4. Book Avaibility\n5. Issued Books\nEnter Choice: ");
        scanf("%d",&choice);
        switch (choice){
            case 1:// Find Book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
                break;
            case 2://edit profile
                edit_profile();
                break;
            case 3://change password
                change_password(u);
                break;
            case 4://Book Available
                bookcopy_checkavail();
                break;
            case 5://list issued books
                display_issued_bookcopies(u->id);
                break;
            }
        }while (choice != 0); 
    }
    void bookcopy_checkavail(){
    int book_id;
    bookcopy_t bc;
    int count=0;
    //input book id
    printf("Enter book id: ");
    scanf("%d",&book_id);
    //open book copies file
    FILE *fp;
    fp = fopen(BOOKCOPY_DB,"rb");
    if(fp == NULL){
        perror("can not open copies of this");
        return;
    }
    //open book copy record one by one
    while (fread(&bc, sizeof(bookcopy_t), 1 ,fp) > 0){
        //if id is matching and book copy is available, count the copies.
        if(bc.bookid == book_id && strcmp(bc.status , STATUS_AVAIL)==0){
           // bookcopy_display(&bc);
            count++;
        }
    }
    //close book copies file
    fclose(fp);
    // print the message number of copiesa available
    printf("no copies available.: %d\n", count);
    

}
